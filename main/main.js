function filterBy(arr, type) {
    let filteredArray = arr.filter( (element) => {return typeof(element) !== type} );
    return filteredArray;
}

let id = Symbol();
let user = {
    name: "Alex",
    surname: "Edison",
    age: 20,
};

let array = ['hello', 'world', undefined, 1234567890123456789012345678901234567890n,
                true, 23, '23', user, id, false, null];
console.log(array);

let type1 = "string";
let type2 = "number";
let type3 = "boolean";
let type4 = "object";
let type5 = "bigint";
let type6 = "symbol";
let type7 = "undefined";

let newArray = filterBy(array, type1);
console.log(newArray);